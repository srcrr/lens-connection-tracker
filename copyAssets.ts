const path = require('path');
const fs = require('fs');
const moment = require("moment");
import { Dirent, Dir, fstatSync, fstat, Stats } from 'fs';
import { Moment } from 'moment';

async function findFiles(root : string, pattern : RegExp) {
    let results = new Array<string> ();
    let dir = await fs.promises.opendir(root);
    for await (let dirent of dir) {
        dirent = dirent as Dirent;
        let nextPath = path.resolve(root, (<Dirent> dirent).name);
        if ((<Dirent> dirent).isDirectory()) {
            (await findFiles(nextPath, pattern)).forEach((pth : string) => {
                results.push(pth);
            })
        } else if ((<Dirent> dirent).isFile()) {
            if ((<Dirent> dirent).name.match(pattern)) {
                results.push(nextPath);
            }
        }
    }
    return results;
}

function removeCommonPath(commonPath : string, paths : string []) {
    return paths.map(pth => pth.replace(commonPath + "/", ''));     // TODO: make windows friendly.
}

function dateLastMod(path : string) : Date | null {
    try {
        let f = <number> fs.openSync(path);
        let mTime = (<Stats> fs.fstatSync(f)).mtime;
        fs.closeSync(f);
        return mTime;
    } catch (err) {
        return null;
    }
}

(async () => {
    const HERE = path.resolve(__dirname);
    let fromDir = path.resolve(HERE, 'src');
    let toDir = path.resolve(HERE, 'build');

    let jsonFiles = removeCommonPath(fromDir, await findFiles(fromDir, /.+\.json$/));
    let htmlFiles = removeCommonPath(fromDir, await findFiles(fromDir, /.+\.html$/));
    let cssFiles = removeCommonPath(fromDir, await findFiles(fromDir, /.+\.css$/));
    let jsFiles = removeCommonPath(fromDir, await findFiles(fromDir, /.+\.js$/));
    let fontFiles = removeCommonPath(fromDir, await findFiles(fromDir, /.+\.eot|ijmap|svg|tff|woff|woff2$/));
    let imageFiles = removeCommonPath(fromDir, await findFiles(fromDir, /.+\.(png|jpg|tif)$/i));
    let codepoints = removeCommonPath(fromDir, await findFiles(fromDir, /^codepoints$/i));

    let allFiles = jsonFiles.concat(htmlFiles).concat(cssFiles).concat(imageFiles).concat(jsFiles).concat(fontFiles).concat(codepoints);
    allFiles.forEach( async (file : string) => {
        let fromPath = path.resolve(fromDir, file);
        let toPath = path.resolve(toDir, file);
        let fromDate = dateLastMod(fromPath);
        let toDate = dateLastMod(toPath);
        if (toDate != null && !moment(fromDate).isAfter(moment(toDate)))
            return;
        if (!fs.existsSync(path.dirname(toPath)))
            await fs.promises.mkdir(path.dirname(toPath), {recursive: true});
        console.log(`copy ${fromPath} => ${toPath}`);
        fs.copyFileSync(fromPath, toPath);
    })

})();