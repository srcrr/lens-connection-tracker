const path = require('path');
const fs = require('fs');
import { Dirent, Dir } from 'fs';
import archiver from 'archiver';
const PACKAGE = require('./package.json');

async function packageDist(buildPath?: string) {
    buildPath = buildPath || path.resolve(__dirname, "build");
    let buildDir = await fs.promises.opendir(buildPath);
    let distDir = path.resolve(__dirname, "dist");
    if (!fs.existsSync(buildPath)) fs.mkdirSync(buildPath);
    if (!fs.existsSync(distDir)) fs.mkdirSync(distDir);
    let zipName = `${PACKAGE.name}-${PACKAGE.version}.zip`
    var output = fs.createWriteStream(path.resolve(distDir, zipName));
    var archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
    });
    output.on('close', function () {
        console.log(archive.pointer() + ' total bytes');
        console.log('archiver has been finalized and the output file descriptor has closed.');
    });

    archive.pipe(output);
    archive.directory(buildPath, false);

    archive.finalize()
}

(async () => {
    packageDist();
})();