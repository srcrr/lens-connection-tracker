import { Browser as RealBrowser, MailFolder, MessageList, MessageHeader, MessagePart, MailAccount } from "../lib/browser";

import { FakeThunderbirdBrowser } from "../lib/fake/browser";

type Browser = RealBrowser | FakeThunderbirdBrowser;

export class MessageProxy {
    private browser : Browser;
    public header : MessageHeader;

    public async getPart(): Promise<MessagePart | null> {
        if (!this.browser.messages) {
            return null;
        }
        return this.browser.messages.getFull(this.header.id);
    }

    constructor(browser : Browser, header: MessageHeader) {
        this.browser = browser;
        this.header = header;
    }
}

export async function getAccounts(browser: Browser): Promise<MailAccount[]> {
    if (!browser.accounts) {
        return new Array<MailAccount>();
    }
    
    if ('accounts' in browser)
        return browser.accounts.list()
    return new Array<MailAccount>();
}

export async function getFolders(browser: Browser, accounts?: MailAccount[]): Promise<MailFolder[]> {
    accounts = accounts || await getAccounts(browser);
    let allFolders = new Array<MailFolder>();
    accounts.forEach((account) => {
        console.log("account:");
        console.log(account);
        allFolders = allFolders.concat(account.folders);
    })

    return allFolders;
}

export async function getMessages(browser: Browser, folders?: MailFolder[]) : Promise<MessageProxy []>{
    if (!browser.messages) {
        return [];
    }
    folders = folders || await getFolders(browser);
    let messages = new Array<MessageProxy>();
    function addMessage(message: MessageHeader) {
        console.log("message: ");
        console.log(message);
        messages.push(new MessageProxy(browser, message));
    }
    folders.forEach(async (folder) => {
        if (!browser.messages)
            return null;
        console.log("folder: ");
        console.log(folder);
        let page = await browser.messages.list(folder);
        page.messages.forEach(addMessage);
        while (page.id) {
            page = await browser.messages.continueList(page.id);
            page.messages.forEach(addMessage);
        }
    });
    return messages;
}

export async function authorIsMe(browser : Browser, header: MessageHeader) {
    if (!browser.accounts) {
        return false;
    }
    let accounts = await browser.accounts.list();
    return accounts.reduce( (previousValue : any, currentValue : MailAccount) => {
        return previousValue || (
            currentValue.type == "imap" &&
            currentValue.name.indexOf(header.author) > 0
        );
    } );
}