import { Port, Browser } from "lib/browser";
import { ListenerFuncs } from "./types";
import { CHANNEL_NAME } from ".";
import { MessageListener } from "./MessageListener";

// // Example Listener

// // content-script.js

// let myPort = browser.runtime.connect({name:"port-from-cs"});
// console.log("myPort:")
// console.dir(myPort);
// myPort.postMessage({greeting: "hello from content script"});

// myPort.onMessage.addListener(function(m) {
//   console.log("In content script, received message from background script: ");
//   console.log(m.greeting);
// });

// document.body.addEventListener("click", function() {
//   myPort.postMessage({greeting: "they clicked the page!"});
// });

// // END example listener


export class ContentMessageListener extends MessageListener {

  name: () => string = () => {
    return "ContentMessageListener";
  };

  private port: Port | null;

  constructor(
    public browser: Browser,
    public listeners: ListenerFuncs,
  ) {
    super(browser, listeners);
    this.port = null;
  }

  get listenerNames() {
    return Object.keys(this.listeners);
  }

  public async connect() {
    console.log(`Connecting ContentMessageListener`);
    this.browser.runtime.onConnect.addListener((port : Port) => {
      console.log(`Connected to ${port.name}`);
    })
    
    // this.port = this.browser.tabs.connect(this.browser);
    if (!this.port)
      throw new Error(`Could not get ContentMessageListener port`);
    // this.port.postMessage({ greeting: "content script says hello" })
    this.port.onMessage.addListener(this.onMessage);
    this.port.onDisconnect.addListener(this.onDisconnect);
    console.log(`✅ ContentMessageListener.connect: Success`);
    return this;
  }

  public sendToBackground = (message: any) => {
    console.log(`Content Sending message: ${JSON.stringify(message)}`);
    try {
      if (!this.port)
        throw new Error(`ContentMessageListener: Port is null. Ensure you run connect()`);
      this.port.postMessage(message);
      console.log(`posting message complete`);
      return this;
    } catch (err) {
      throw new Error(`Sending to background: ${err}`);
    }
  }
  onDisconnect = (port: Port) => {
      console.warn(`ContentListener disconnected: ${JSON.stringify(port)}`);
  }
}
