import { Browser, AddressBookNode, ContactNode } from "../lib/browser";
import * as chai from "chai";
import deepEqualInAnyOrder from 'deep-equal-in-any-order';
import { stringify } from "querystring";
import { FakeThunderbirdBrowser } from "../lib/fake/browser";

chai.use(deepEqualInAnyOrder);

export class TestResult {
    constructor (
        public title : string,
        public pass : boolean,
        public error? : Error,
    ) {}
}

export type Func = () => void;
export type AsyncFunc = () => Promise<void>;

export type TestList = {
    [key: string] : Func | AsyncFunc,
}

export async function unitTest(browser: Browser | FakeThunderbirdBrowser) : Promise <TestResult []>{
    let tests = {
        "Browser is not null" : () => {
            chai.assert.isNotNull(browser);
        },
        "Appropriate permissions have been granted": async () => {
            chai.assert.isNotNull(browser.permissions);
            if (!browser.permissions) {
                return;
            }
            let expectedPermissions = [
                "activeTab",
                "tabs",
                "tabHide",
                "accountsRead",
                "addressBooks",
                "storage",
                "unlimitedStorage",
                "messagesRead"
            ];
            console.log("permissions:");
            let actualPermissions = await browser.permissions.getAll();
            console.log(actualPermissions);
            chai.expect(actualPermissions.permissions).to.deep.equalInAnyOrder(expectedPermissions);
        },
        "Can store a value in local storage" : async () => {
            let toStore = {"SomeKey" : "SomeValue"};
            browser.storage.local.set(toStore);
            let val = await browser.storage.local.get("SomeKey");
            console.log(`toStore is "${stringify(toStore)}"`);
            console.log(`val is ${val}`);
            chai.assert.deepStrictEqual(toStore, val);
        },
        // "Can remove a value from local storage": async () => {
        //     let toStore = {"SomeKey" : "SomeValue"};
        //     browser.storage.local.set(toStore);
        //     browser.storage.local.remove("SomeKey");
        //     let val = await browser.storage.local.get("SomeKey");
        //     chai.assert.deepEqual(val, {});
        // },
        "Browser contains at least 1 account.": async () => {
            chai.assert.isNotNull(browser.accounts);
            if (!browser.accounts) {
                return;
            }
            // let size = 0;
            console.log('accounts:');
            console.log(browser);
            console.log(browser.accounts);
            chai.assert.isDefined(browser.accounts);
            let size = (await browser.accounts.list()).length;
            chai.assert.isAtLeast(size, 1);
        },
        "_addressbooks should be defined" : async () => {
            chai.assert.isNotNull(browser.addressBooks);
            if (!(browser instanceof FakeThunderbirdBrowser))
                return;
            if (!(browser.addressBooks && browser.addressBooks._addressBooks))
                return null;
            chai.assert.isDefined(browser.addressBooks._addressBooks);
        }
    } as TestList;

    let testResults = new Array<TestResult>();

    for (let testKey in tests) {
        let fn = tests[testKey];
        try {
            let res = fn();
            if (res instanceof Promise)
                await res;
            testResults.push(new TestResult(testKey, true));
        } catch (err) {
            testResults.push(new TestResult(testKey, false, err));
            console.log(err);
        }
    }
    return testResults;
}