import { MailAccount, _MailFolder, FakeThunderbirdBrowser, _Accounts, _Folders, _Runtime, _Messages, _MessageHeader, _ContactNode, _MessagePart, _Contacts, _AddressBooks, _Extension, _Tabs, _Storage } from "lib/fake/browser";
import * as uuid from 'uuid';
import * as faker from "faker";
// const faker = require("faker");
import { Browser, ContactNode, AddressBookNode } from "lib/browser";
import { chooseRandom, chooseManyRandom, fakeMessageHeader, fakeMessagePart } from "lib/devtools";
import moment from "moment";

export async function fakeProfile(real: Browser) {
    let selfEmail = faker.internet.email();
    // let browser = new FakeThunderbirdBrowser(real);
    let companies = [] as (string | null)[];
    for (let i = 0; i < 50; ++i) {
        if (Math.random() < 0.3)
            companies.push(faker.company.companyName());
        else
            companies.push(null);
    }
    let mailAccountId = uuid.v4();
    let folders = {
        root : new _MailFolder(mailAccountId, "/ROOT", "root"),
        inbox: new _MailFolder(mailAccountId, "INBOX", "Inbox"),
        sent: new _MailFolder(mailAccountId, "SENT", "Sent"),
    }

    let _foldersObj = new _Folders(folders.root);
    if (!_foldersObj._root) {
        _foldersObj._root = new _MailFolder(mailAccountId, "/", "ROOT");
    }
    _foldersObj._root._children = [
        folders.inbox,
        folders.sent,
    ];

    let mailAccounts = [
        {
            name: "Fake IMAP Account",
            id: mailAccountId,
            type: "imap",
            folders: Object.values(folders),
        } as MailAccount,
    ]

    // Create an address book.
    let addressBook = {
        id: uuid.v4(),
        name: "Fake Address Book",
    } as AddressBookNode;

    addressBook.contacts = new Array<_ContactNode>();

    // Create a bunch of contacts
    let _contacts = new Array<_ContactNode>();
    for (let i = 0; i < 100; ++i) {
        let firstName = faker.name.firstName();
        let lastName = faker.name.lastName();
        let contact = {
            id: uuid.v4(),
            properties: {
                FirstName: firstName,
                LastName: lastName,
                PrimaryEmail: faker.internet.email(firstName, lastName),
                Company: companies[Math.random() * companies.length],
            },
            parentId: addressBook.id,
        } as _ContactNode
        _contacts.push(contact);
        addressBook.contacts.push(contact)
    }

    let runtime = new _Runtime(real);

    let messages = new _Messages();

    let beginDate = moment().subtract(1, "year");
    let endDate = moment();

    // Fill out the inbox.
    let folder = folders.inbox;
    for (let i = 0; i < 100; ++i) {
        let messageHeader = fakeMessageHeader(false, i, beginDate, endDate, _contacts, folder, selfEmail);
        let messagePart = fakeMessagePart(messageHeader);
        messages._mailHeaders.push(messageHeader);
        messages._mailParts.push(messagePart);
    }

    // Fill out the sent box.
    folder = folders.sent;
    for (let i = 0; i < 100; ++i) {
        let messageHeader = fakeMessageHeader(true, i, beginDate, endDate, _contacts, folders.inbox, selfEmail);
        let messagePart = fakeMessagePart(messageHeader);
        messages._mailHeaders.push(messageHeader);
        messages._mailParts.push(messagePart);
    }

    let contacts = new _Contacts();
    contacts._contacts = _contacts;

    let _addressBooks = [{
        id: uuid.v4(),
        name: "Fake Address Book",
        type: "addressBook",
        contacts: _contacts,
    } as AddressBookNode
    ]

    let addressBooks = new _AddressBooks(JSON.parse(JSON.stringify(_addressBooks)));

    let extension = new _Extension();
    let tabs = new _Tabs(real);
    let storage = new _Storage(real);

    let accounts = new _Accounts(mailAccounts);
    accounts._mailAccounts = JSON.parse(JSON.stringify(mailAccounts));
    console.log("Adding accounts");
    console.log(accounts);

    let fakeBrowser = new FakeThunderbirdBrowser(
        real,
        accounts,
        Object.values(folders),
        runtime,
        _foldersObj,
        messages,
        contacts,
        addressBooks,
        extension,
        tabs,
        storage,
    );
    return fakeBrowser;
}