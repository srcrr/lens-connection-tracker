const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

function srcPath(subdir) { return path.join(__dirname, "src", subdir); }

module.exports = {
    mode: process.env.NODE_ENV || "production",
    entry: {
        background: './src/background.ts',
        index: './src/index.js',
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.ts?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            },
        ],
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
    resolve: {
        alias: {
            lib: srcPath("lib"),
            views: srcPath("views"),
            controllers: srcPath("controllers"),
        },
        extensions: ['.tsx', '.ts', '.js', '.vue'],
        // alias: { vue: 'vue/dist/vue.esm.js' },
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'build'),
    },
};