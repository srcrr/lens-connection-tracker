/// <reference types="./types.d.ts">
import $ from 'jquery';
import M from "materialize-css";
import Vue from "vue";
// import initMessenger from "./lib";
import AsyncComputed from 'vue-async-computed';
// import AsyncData from 'vue-async-data-2';

import { GlobalThis, Browser } from 'lib/browser';
import { ListenerFuncs } from 'lib/util/messenger/types';
import { IMessageListener } from 'lib/util/messenger/IMessageListener';
import { getIsDirty, cleanDirty } from 'lib/util';

Vue.use(AsyncComputed);
// Vue.use(AsyncData);

// Vue.prototype.$browser = globalThis.browser;

async function checkRefresh(browser : Browser, window : Window) {
    if (await getIsDirty(browser)) {
        console.log(`Browser is dirty. Reloading`);
        await cleanDirty(browser);
        window.location.reload();
    }
}

export async function init(globalThis: GlobalThis, App: Vue.Component) {

    if (! globalThis.browser) {
        throw new Error(`Browser check failed`);
    }

    $(document).ready(() => {
        M.AutoInit();
        $("#windowRefresh").click(() => {
            globalThis.window.location.reload();
        });
        new Vue({
            render: h => h(App),
        }).$mount("#app");
    });

    setInterval(checkRefresh, 100, globalThis.browser, globalThis.window);
}