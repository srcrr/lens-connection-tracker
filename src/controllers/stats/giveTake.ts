import { Browser as RealBrowser, ContactNode, MessageHeader } from "lib/browser";
import { FakeThunderbirdBrowser, _QueryInfo } from "lib/fake/browser";
import { Chart, ChartData, ChartOptions, ChartColor, ChartDataSets } from "chart.js";
import { ProgressListener, Statistics, BeatPoints, TimeDataDef, TimeData } from "lib/analytics";
import { StatisticsRunner } from "lib/StatisticsRunner";
import { ContentMessageListener } from "lib/util/messenger/ContentMessageListener";
import { ListenerFunc } from "lib/util/messenger/types";
import moment from "moment";
import { userIsRecipient, userIsAuthor, findMessageByInternalId } from "lib/util";

type Browser = RealBrowser;

function randomColor(alpha?: number) {
    let r = Math.round(Math.random() * 256);
    let g = Math.round(Math.random() * 256);
    let b = Math.round(Math.random() * 256);
    let a = alpha || Math.round(Math.random());
    return `rgba(${r}, ${g}, ${b}, ${a})`;
}

function getHourLabels () : string [] {
    let labels = new Array<string>(24);
    labels[0] = "12 am";
    labels[12] = "12 am";
    for (let i = 0; i <= 24; ++i) {
        let h = i;
        let p = 'am'
        if (i >= 12) {
            h = h - 12
            p = 'pm'
        }
        if (h == 0) {
            h = 12
        }
        labels[i] = `${h} ${p}`
    }
    return labels;
}

export async function getGiveTakeBarChart(browser: Browser, ctx: CanvasRenderingContext2D) {

    if (!browser) {
        throw new Error(`browser`);
    }

    if (!browser.accounts) {
        throw new Error(`browser.accounts`);
    }

    if (!browser.messages) {
        throw new Error(`Browser is missing 'messages'`);
    }
    let statistics = await Statistics.fromLocalStorage(browser);
    if (!statistics) {
        throw new Error(`Could not fetch 'Statistics' from storage`);
    }

    return new Chart(ctx, {
        type: "bar",
        data: {
            // labels: labels,
            // datasets: dataSets,
        },
    });
}

type VueUpdaterFunc = (current: number, total: number, data?: any) => void;
type RunBeginFunc = (runner: StatisticsRunner) => void;

export function tsRunStatsCollection(messageListener: ContentMessageListener) {
    messageListener.connect();
    messageListener.sendToBackground({ command: "RunStatistics" });
}