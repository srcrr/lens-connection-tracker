import { Browser as RealBrowser, ContactNode } from "lib/browser";
import { FakeThunderbirdBrowser, _QueryInfo } from "lib/fake/browser";
import { Chart, ChartData, ChartOptions, ChartColor } from "chart.js";
import { ProgressListener, Statistics, BeatPoints } from "lib/analytics";
import { StatisticsRunner } from "lib/StatisticsRunner";
import { ContentMessageListener } from "lib/util/messenger/ContentMessageListener";
import { ListenerFunc } from "lib/util/messenger/types";

type Browser = RealBrowser;

function randomColor(alpha?: number) {
    let r = Math.round(Math.random() * 256);
    let g = Math.round(Math.random() * 256);
    let b = Math.round(Math.random() * 256);
    let a = alpha || Math.round(Math.random());
    return `rgba(${r}, ${g}, ${b}, ${a})`;
}

export async function getContactPieChart(browser: Browser, ctx: CanvasRenderingContext2D, direction: "sent" | "received", N? : number) {

    N = N || 50;

    if (!browser.contacts) {
        throw new Error(`Browser does not have contacts`);
    }

    let data = {
        labels: new Array<string>(),
        datasets: [
            {
                label: "Messages",
                data: new Array<number>(),
                backgroundColor: new Array<ChartColor>(),
            },
        ]
    }

    let stats = (await browser.storage.local.get() as any).Statistics as Statistics;

    if (!stats) {
        console.error(`Stats have not been generated`);
        return null;
    }
    type _BeatPair = [string, { sent: BeatPoints; received: BeatPoints; }];

    let sortedBeats = Object.entries(stats.beats).filter((item: _BeatPair) => {
        return item != null && item != undefined;
    });

    sortedBeats = sortedBeats.sort((beat1: _BeatPair, beat2: _BeatPair) => {
        // console.log(`beats: ${beat1}, ${beat2}`);
        if (!(beat1[1][direction] && beat2[1][direction])) return 0;
        let l1 = Object.values(beat1[1][direction]).length;
        let l2 = Object.values(beat2[1][direction]).length;
        return l1 - l2;
    });

    sortedBeats.reverse();

    // Gets top 20 contacts
    for (let beat of sortedBeats.filter((arr, i) => { return i < (N || 50) })) {
        let contactId = beat[0];
        let label : string | null = null;
        let point : number | null = null;
        if (contactId.startsWith("bucket://")) {
            let bucket = ((await browser.storage.local.get() as any).ContactBucket || new Array<string>()) as string [];
            let bucketI = Number(contactId.substr("bucket://".length));
            if (bucketI > bucket.length-1) {
                console.log(`${bucketI} is greater than bucket length ${bucket.length}`);
                continue;
            }
            label = bucket[bucketI];
            point = Object.values(beat[1][direction]).length
        } else {
            let contact = (await browser.contacts.get(contactId));
            if (!contact) {
                console.error(`Could not find contact with ID ${contactId}`);
                continue;
            }
            let displayName = contact.properties.DisplayName;
            let firstName = contact.properties.FirstName;
            let lastName = contact.properties.LastName;
            let email = contact.properties.PrimaryEmail || contact.properties.SecondEmail;
            let label: string;
            if (displayName) {
                label = displayName;
            } else if (firstName) {
                label = firstName;
                if (lastName) label += " " + lastName;
            } else if (email) {
                label = email;
            } else {
                console.error(`Could not get any contact info for ${JSON.stringify(contact)}`);
                continue;
            }
            if (!(beat && beat[1] && beat[1][direction])) continue;
            point = Object.values(beat[1][direction]).length
        }
        if (!(point && label)) {
            continue;
        }
        data.labels.push(label);
        data.datasets[0].data.push(point);
        data.datasets[0].backgroundColor.push(randomColor(1.0));
    }

    return new Chart(ctx, {
        type: "doughnut",
        data: data,
        options: {
            legend: {
                display: true,
            }
        }
    });
}

type VueUpdaterFunc = (current: number, total: number, data?: any) => void;
type RunBeginFunc = (runner: StatisticsRunner) => void;

export function tsRunStatsCollection(messageListener: ContentMessageListener) {
    messageListener.connect();
    messageListener.sendToBackground({ command: "RunStatistics" });
}