import { Port, Browser } from "lib/browser";
import { ListenerFunc, ListenerFuncs } from "./types";
import { IMessageListener } from "./IMessageListener";
import { MessageListener } from "./MessageListener";

// // Example listener from https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Content_scripts#Communicating_with_background_scripts

// let portFromCS : Port | null;

// function connected(p : Port | null) {
//   portFromCS = p;
//   portFromCS?.postMessage({greeting: "hi there content script!"});
//   portFromCS?.onMessage.addListener(function(m : any) {
//     console.log("In background script, received message from content script");
//     console.log(m);
//   });
// }

// browser.runtime.onConnect.addListener(connected);

// browser.browserAction.onClicked.addListener(function() {
//   portFromCS?.postMessage({greeting: "they clicked the button!"});
// });

// // END example listener

export class BackgroundMessageListener extends MessageListener {
  // background-script.js

  public name () {
    return "BackgroundMessageListener";
  };

  private listenPort: Port | null;

  public connected(p: Port | null) {
    this.listenPort = p;
    this.listenPort?.postMessage({ greeting: "Connected background script" });
    if (!this.listenPort)
      throw new Error(`👂 BackgroundMessageListener: Could not get port!`);
    console.log(`🔌 Connected to port : ${JSON.stringify(this.listenPort)}`);
    this.listenPort.onMessage.addListener(this.onMessage);
    this.listenPort.onDisconnect.addListener(this.onDisconnect);
  }

  constructor(
    public browser: Browser,
    public listeners: ListenerFuncs,
  ) {
    super(browser, listeners);
    this.listenPort = null;
    this.browser.runtime.onConnect.addListener(this.connected);
  }
  
  public sendToForeground(message: any) {
    if (!this.listenPort) {
      throw new Error(`Background message listener: no port`);
    }
    this.listenPort.postMessage(message);
    return this;
  }

  onDisconnect = (reason: any) => {
      console.warn(`BackgroundListener disconnected: ${JSON.stringify(reason)}`);
  }
}