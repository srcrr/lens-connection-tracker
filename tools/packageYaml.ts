const fs = require('fs');
const path = require('path');
const YAML = require('yaml');
const jsonfile = require('jsonfile');

const ROOT = path.resolve(path.dirname(__filename), '..')
const PACKAGE_YAML = path.resolve(ROOT, 'package.yaml');
const PACKAGE_JSON = path.resolve(ROOT, 'package.json');

let inputJson = require(PACKAGE_JSON);

fs.writeFileSync(PACKAGE_YAML, YAML.stringify(inputJson));