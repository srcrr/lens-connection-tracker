import $ from "jquery";
const BUTTON_ID = "#connectiontracker_damngood_pro-browserAction-toolbarbutton";

import { Browser, AddressBookNode, ContactNode, Port, GlobalThis } from "./lib/browser";
import { getMessages } from "./lib/messages";
import { getBrowser, setDirty } from "./lib/util";
import { StatisticsRunner } from "lib/StatisticsRunner";
import { BackgroundMessageListener } from "lib/util/messenger/BackgroundMessageListener";
import { ListenerFunc } from "lib/util/messenger/types";
import { IMessageListener } from "lib/util/messenger/IMessageListener";
import { Progress } from "lib/analytics";

type CommandDispatcher = { [name: string]: (browser: Browser, port: Port, ...args: any[]) => void }
async function init(browser?: Browser | null) {
  browser = browser || getBrowser(globalThis);

  console.log(`background: start`);

  if (!browser?.accounts) {
    throw new Error(`No accounts in browser`);
  }
  if (!browser.extension) {
    throw new Error(`No extension in browser`);
  }
  if (!browser.tabs) {
    throw new Error(`No tabs in browser`);
  }
  if (!browser.addressBooks) {
    throw new Error(`No addressBooks in browser`);
  }

  browser.runtime.onStartup.addListener(() => {
    console.log(`🏁 STARTUP`);
  });

  // browser.runtime.onSuspend.addListener(() => {
  //   console.log(`💤 SUSPENDED`);
  // });

  // browser.runtime.onSuspendCanceled.addListener(() => {
  //   console.log(`😲 ON SUSPEND CANCELED`);
  // });


  browser.runtime.onMessage.addListener((message: any) => {
    console.log(`📨 MESSAGE: ${JSON.stringify(message)}`);
  });

  browser.runtime.onUpdateAvailable.addListener(() => {
    console.log(`📦 UPDATE AVAILALBE`)
  });

  // browser.runtime.onBrowserUpdateAvailable.addListener(() => {
  //   console.log(`🌏 {BROWSER} UPDATE AVAILALBE`)
  // });

  /**
   * The main command listener
   * @param port Port on which to listen for commands
   */

  let listener = (port: Port) => {
    console.log(`*connected*`);
    console.log(port);
    if (!browser) throw new Error();

    let runner = new StatisticsRunner(browser, null,
      // ProgressListener
      async (current: number, total: number, data?: any) => {
        try {
          port.postMessage({
            status: "running",
            total: total,
            current: current,
          });
        } catch (err) {
          console.warn(err);
          console.log(`Shutting down statistics runner`);
          runner.saveProgress();
          runner.pause();
        }
      },
      // Completion listener
      async () => {
        try {
          port.postMessage({
            status: "complete",
          });
        } catch (err) {
          console.warn(err);
          console.log(`Shutting down statistics runner`);
          runner.saveProgress();
          runner.pause();
        }
      })

    const COMMAND_DISPATCH = {
      async RunStatistics(browser: Browser, port: Port) {
        await runner.resume();
        await runner.run();
      },
      async PauseStatistics(browser: Browser, port: Port) {
        await runner.saveProgress();
        await runner.pause();
        port.postMessage({
          paused: true,
        });
      },
      async SaveStatistics(browser: Browser, port: Port) {
        await runner.saveProgress();
        port.postMessage({
          saved: true,
        });
      },
      async ClearStatistics(browser: Browser, port: Port) {
        await runner.clearAll();
        await setDirty(browser);
      },
    } as CommandDispatcher;

    port.onMessage.addListener((message: any) => {
      console.log(`received message`);
      console.log(message);
      if (!browser) throw new Error(`no browser`);
      if ("command" in message) {
        if (!COMMAND_DISPATCH[message.command]) {
          console.warn(`${message.command} not in COMMAND_DISPATCH`);
        }
        COMMAND_DISPATCH[message.command](browser, port);
      }
    });
  }

  browser.runtime.onConnect.addListener(listener);

  (globalThis as any)['mainBrowser'] = browser;

  browser.browserAction.onClicked.addListener(async () => {
    if (!browser?.extension) {
      throw new Error(`No extension in browser`);
    }
    if (!browser.tabs) {
      throw new Error(`No tabs in browser`);
    }
    browser.tabs.create({ url: browser.extension.getURL("./popup.html") });
  });
  browser.tabs.create({ url: browser.extension.getURL("./popup.html") });

  // This is now dirty
  await setDirty(browser);
};


// CALL THE MAIN FUNCTION
init();