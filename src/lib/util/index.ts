import stringify from "fast-json-stable-stringify";
import { MailFolder, Browser as RealBrowser, Port, MessageHeader, MailAccount, QueryInfo } from "../../lib/browser";
import { FakeThunderbirdBrowser } from "../fake/browser";
import { getOptions } from "controllers/options";
const re = require('re.js');

// type Browser = RealBrowser | FakeThunderbirdBrowser;
type Browser = RealBrowser;

/**
 * https://emailregex.com/
 */
export const EMAIL_REGEXP = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;

export function preferFunctions(originVal: any, newVal: any, key: string) {
  console.log(`browser[${key}] = ${originVal} / ${stringify(newVal)}`);
  return originVal || newVal;
}

export function emailGrep(text: string) {
  return re(EMAIL_REGEXP).match(text)[0];
}

export async function* listMessages(browser: Browser, folder: MailFolder) {
  if (!browser.messages) {
    yield null;
    return;
  }
  let page = await browser.messages.list(folder);
  for (let message of page.messages) {
    yield message;
  }

  while (page.id) {
    page = await browser.messages.continueList(page.id);
    for (let message of page.messages) {
      yield message;
    }
  }
}

export async function* findMessages(browser: Browser, query: QueryInfo) {
  if (!browser.messages) {
    yield null;
    return;
  }
  let page = await browser.messages.query(query);
  for (let message of page.messages) {
    yield message;
  }

  while (page.id) {
    page = await browser.messages.continueList(page.id);
    for (let message of page.messages) {
      yield message;
    }
  }
}


export function getBrowser(_globalThis: any) {
  if (_globalThis.browser)
    return _globalThis.browser as Browser;
  return null;
}


export async function getIsDirty(browser: Browser) {
  return (await browser.storage.local.get('Dirty') as any).Dirty as boolean;
}

export async function cleanDirty(browser: Browser) {
  return (await browser.storage.local.set({ Dirty: false }));
}

export async function setDirty(browser: Browser) {
  return (await browser.storage.local.set({ Dirty: true }));
}

export async function userIsAuthor(browser: Browser, message: MessageHeader) {
  let options = await getOptions(browser);
  return options.Account.myAddresses.includes(message.author);
}

export function recipientsContainAddress(message: MessageHeader, address: string) {
  return message.recipients.some((recipient) => recipient.includes(address));
}

export async function userIsRecipient(browser: Browser, message: MessageHeader) {
  let options = await getOptions(browser);
  return options.Account.myAddresses.some((address) => recipientsContainAddress(message, address));
}

export async function getInternalId(browser: Browser, messageHeader: MessageHeader) {
  // if (!browser.accounts) throw new Error(`browser.accounts`);
  // if (!browser.folders) throw new Error(`browser.accounts`);
  if (!browser.messages) throw new Error(`browser.messages`);
  let full = await browser.messages.getFull(messageHeader.id);
  if (!full) throw new Error(`Could not get full message of ${messageHeader.id}`);
  if (!full.headers) throw new Error(`Message ${messageHeader.id} contains no 'headers'`);
  return full.headers["message-id"];
}

export async function findMessageInListByInternalId(browser: Browser, messageId: string, messages: MessageHeader[]) {
  if (!browser.accounts) throw new Error(`browser.accounts`);
  if (!browser.folders) throw new Error(`browser.accounts`);
  if (!browser.messages) throw new Error(`browser.messages`);
  for (let header of messages) {
    let intId = await getInternalId(browser, header);
    if (intId === messageId) {
      return header;
    }
  }
  return null;
}

export async function findMessageByInternalId(browser: Browser, messageId: string, mailAccounts?: MailAccount[], folders?: MailFolder[]) {
  if (!browser.accounts) throw new Error(`browser.accounts`);
  if (!browser.folders) throw new Error(`browser.accounts`);
  if (!browser.messages) throw new Error(`browser.messages`);

  if (!mailAccounts) {
    mailAccounts = await browser.accounts.list();
  }
  if (!folders) {
    folders = new Array<MailFolder>();
    for (let account of mailAccounts) {
      for (let f of account.folders)
        folders.push(f);
    }
  }
  for (let folder of folders) {
    let page = await browser.messages.list(folder);
    let msg = findMessageInListByInternalId(browser, messageId, page.messages);
    if (msg) return msg;
    while (page.id) {
      page = await browser.messages.continueList(page.id);
      let msg = findMessageInListByInternalId(browser, messageId, page.messages);
      if (msg) return msg;
    }
  }
  return null;
}