
export type NodeType = "addressBook" | "contact" | "mailingList";

export type MailFolder = {
  accountId: string,
  path: string,
  name?: string,
  type?: string,

  // Added for compatibility for fake._QueryInfo
  _children?: any,
  _addChild: any,
  _delete: any,
  toJson: any,
}

export type MailAccount = {
  folders: MailFolder[],
  id: string,
  name: string,
  type: "imap" | "nntp" | "pop3",
}

export type MessageHeader = {
  author: string,
  bccList?: string[],
  ccList?: string[],
  date: Date,
  flagged?: boolean,
  folder?: MailFolder,
  id: number,
  read: boolean,
  recipients: string[],
  subject: string,
  tags?: string[]
}

export type MessagePart = {
  body?: string,
  contentType?: string,
  headers?: {[key: string] : any},
  name?: string,
  partName?: string,
  parts?: MessagePart,
  size?: number,
}

export type MessageTag = {
  color: string,
  key: string,
  ordinal?: string,
  tag: string,
}

export type MessageList = {
  id: string | null,
  messages: MessageHeader[],
}

export type QueryInfo = {
  author?: string,
  body?: string,
  flagged?: boolean,
  folder?: MailFolder,
  fromDate?: Date,
  fromMe?: boolean,
  fullText?: string,
  recipients?: string,
  subject?: string,
  toDate?: Date,
  toMe?: boolean,
  unread?: boolean,
}

export type ContactProperties = {
  FirstName?: string,
  LastName?: string,
  PhoneticFirstName?: string,
  PhoneticLastName?: string,
  DisplayName?: string,
  NickName?: string,
  SpouseName?: string,
  FamilyName?: string,
  PrimaryEmail?: string,
  SecondEmail?: string,
  HomeAddress?: string,
  HomeAddress2?: string,
  HomeCity?: string,
  HomeState?: string,
  HomeZipCode?: string,
  HomeCountry?: string,
  HomePhone?: string,
  HomePhoneType?: string,
  FaxNumber?: string,
  FaxNumberType?: string,
  PagerNumber?: string,
  PagerNumberType?: string,
  CellularNumber?: string,
  CellularNumberType?: string,
  JobTitle?: string,
  Department?: string,
  Company?: string,
  _AimScreenName?: string,
  AnniversaryYear?: string,
  AnniversaryMonth?: string,
  AnniversaryDay?: string,
  BirthYear?: string,
  BirthMonth?: string,
  BirthDay?: string,
  WebPage1?: string,
  WebPage2?: string,
  Custom1?: string,
  Custom2?: string,
  Custom3?: string,
  Custom4?: string,
  Notes?: string,
  PopularityIndex?: string,
  PreferMailFormat?: string,
  PhotoName?: string,
  PhotoType?: string,
  PhotoURI?: string,
};

export type Tab = {};

export type GlobalThis = {
  browser?: Browser,
  window: Window,
  indexedDB: IDBFactory,
};

export type ContactNode = {
  id: string,
  properties: ContactProperties,
  type: NodeType,
  parentId?: string,
  readOnly?: boolean,
};

export type MailingListNode = {
  description: string,
  id: string,
  name: string,
  nickname: string,
  type: NodeType,
  contacts?: ContactNode[],
  parentId?: number,
  readOnly?: boolean,
};

export type AddressBookNode = {
  id: string,
  name: string,
  type: NodeType,
  contacts: ContactNode[],
  mailinglists: MailingListNode[],
  parentId?: string,
  readOnly?: boolean,
};

export type RuntimeListener = {
  addListener: ((... args : any []) => any),
  removeListener: ((... args : any []) => any),
  hasListener: ((... args : any []) => any),
};

export type Port = {
  name: string,
  disconnect: () => void,
  onMessage: MediaQueryList,
  postMessage: (message: any) => void,
  error: object,
  onDisconnect: RuntimeListener,
  sender: any,
};

export type BrowserPermissions = {
  origins: string | RegExp[],
  permissions: string[],
};

export type WindowType = "normal" | "popup" | "panel" | "devtools";
export type TabStatus = "loading" | "complete";

export type QueryObj = {
  active? : boolean,
  audible? : boolean,
  autoDiscardable? : boolean,
  cookieStoreId? : string,
  currentWindow? : boolean,
  discarded? : boolean,
  hidden? : boolean,
  highlighted ? : boolean,
  index ? : number,
  muted ? : boolean,
  lastFocusedWindow ? : boolean,
  pinned ? : boolean,
  status ? : TabStatus,
  title ? : string,
  url? : string | string [],
  windowId? : number,
  windowType? : WindowType,
};

export type Browser = {
  browserAction?: any,
  permissions?: {
    getAll: () => Promise<BrowserPermissions>,
  },
  accounts?: {
    list: () => Promise<MailAccount[]>,
    get: (accountId: string) => MailAccount,
  },
  runtime: {
    onMessage: RuntimeListener,
    onConnect: RuntimeListener,
    onStartup: RuntimeListener,
    onInstalled: RuntimeListener,
    onSuspend: RuntimeListener,
    onSuspendCanceled: RuntimeListener,
    onUpdateAvailable: RuntimeListener,
    onBrowserUpdateAvailable: RuntimeListener,
    connectNative: (signal: string) => Port | null,
    connect: (args: any) => Port | null,
  },
  folders?: {
    create: (parentFolder: MailFolder, childName: string) => void,
    rename: (folder: MailFolder, newName: string) => void,
    delete: (folder: MailFolder) => void,
  },
  messages?: {
    list: (folder: MailFolder) => Promise<MessageList>,
    continueList: (messageListId: string) => Promise<MessageList>,
    get: (messageId: number) => Promise<MessageHeader | null>,
    getFull: (messageId: number) => Promise<MessagePart | null>,
    query: (queryInfo: QueryInfo) => Promise<MessageList>,
  },
  contacts?: {
    list: (parentId: string) => Promise<ContactNode[]>,
    create: (parentId: string, id?: string | null, properties?: ContactProperties) => Promise<string>,
    get: (id: string) => Promise<ContactNode | null>,
  }
  addressBooks?: {
    openUI: () => void,
    closeUI: () => void,
    get: (id: string, complete?: boolean) => Promise<AddressBookNode>,
    list: ((complete?: boolean) => Promise<AddressBookNode[]>),
    create: (properties: { name?: string }) => Promise<string>,
    update: (id: string, properties: { name?: string }) => void,
    delete: (id: string) => void,
  },
  extension?: {
    getURL: (url: string) => string,
  },
  tabs?: {
    get: (tabId: number) => Promise<Tab | null>,
    getCurrent: () => Promise<Tab | null>,
    create: (createProperties: { active?: boolean, index?: number, url?: string, windowId?: number }) => Promise<Tab | null>,
    duplicate: (tabId: number) => Promise<Tab | null>,
    connect: (tabId: number, connectInfo?: object) => Port | null,
    query: (queryObj: QueryObj) => Promise<Tab[]>,
  },
  connect?: {
    tabId: number,
    connectInfo: number,
  },
  storage: {
    local: {
      set: (keys: object) => Promise<void>,
      get: (keys?: null | string | object | string[]) => Promise<object>,
      remove: (keys?: null | string | object | string[]) => Promise<void | null>,
    }
  }
};