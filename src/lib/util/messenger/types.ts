import { IMessageListener } from "./IMessageListener";

export type AsyncListenerFunc = (m: any) => Promise<null | void>;
export type SyncListenerFunc = (m: any) => null | void;
export type ListenerFunc = AsyncListenerFunc | SyncListenerFunc;

export type ListenerFuncs = {[name : string] : ListenerFunc}