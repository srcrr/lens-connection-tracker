import { Browser } from "lib/browser";
import { IMessageListener } from "./IMessageListener";
import { ListenerFuncs } from "./types";
export abstract class MessageListener implements IMessageListener {

    abstract name() : string;

    constructor(
        public browser: Browser,
        public listeners: ListenerFuncs,
    ) {}

    protected onMessage (message: any) {
        console.log(`received message: ${JSON.stringify(message)}`);
        for (let listenerName of Object.keys(this.listeners)) {
            console.log(`listener ${listenerName} with message`);
            this.listeners[listenerName](message);
        }
    }
}
