import { Browser, ContactNode, AddressBookNode } from "../lib/browser";

export async function getAddressBooks(browser : Browser) : Promise<AddressBookNode[]> {
    if (!(browser && browser.addressBooks)) {
        throw new Error(`Expected browser to have 'addressBooks' property`);
    }
    console.log("here 1");
    if (!("addressBooks" in browser)) {
        console.log("here 2");
        return new Array<AddressBookNode>();
    }
    console.log("here 3");
    console.log(await browser.addressBooks.list());
    return await browser.addressBooks.list();
}

export function getAddressBooksSync(browser : Browser) : AddressBookNode [] {
    let addressBooks = new Array<AddressBookNode>();
    getAddressBooks(browser).then( (books : AddressBookNode []) => {
        addressBooks = books;
    }).catch( (err) => {
        console.error("Could not get address books");
        console.error(err);
    });
    return addressBooks;
}