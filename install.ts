const path = require('path');
const fs = require('fs');
import { Dirent, Dir } from 'fs';
import archiver from 'archiver';
const PACKAGE = require('./package.json');
const MANIFEST = require('./src/manifest.json');

async function installExtension() {
    let zipName = `${PACKAGE.name}-${PACKAGE.version}.zip`;
    let packagePath = path.resolve(__dirname, 'dist', zipName);
    let targetName = `${MANIFEST.applications.gecko.id}.xpi`;
    if (!fs.existsSync(packagePath)) {
        console.error(`Could not find package file ${packagePath}`);
        console.error(`Please ensure you have run 'npm run dist'`);
        return;
    }
    let instTargetPath = process.env.TB_EXTENSION_PATH;
    if (!instTargetPath) {
        console.error("Please install the extension manually and set `TB_EXTENSION_PATH`");
        return 0;
    }
    if (!fs.existsSync(path.resolve(instTargetPath, targetName))) {
        console.error("You have not manually installed the extension yet.");
        console.error(`Please open Thunderbird and install ${packagePath}`)
        return;
    }
    let toPath = path.resolve(instTargetPath, targetName);
    fs.copyFileSync(packagePath, toPath);
}

(async () => {
    await installExtension();
})();