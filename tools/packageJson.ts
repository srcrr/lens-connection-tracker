const fs = require('fs');
const path = require('path');
const YAML = require('yaml');

const ROOT = path.resolve(path.dirname(__filename))
const PACKAGE_YAML = path.resolve(ROOT, 'package.yaml');
const PACKAGE_JSON = path.resolve(ROOT, 'package.json');

const inputYaml = fs.readFileSync(PACKAGE_YAML, 'utf-8');

console.log(YAML.parse(inputYaml));