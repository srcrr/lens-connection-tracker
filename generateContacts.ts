const fs = require('fs');
const path = require('path');
import faker from 'faker';
import vCard from 'vcf';
import axios from "axios";
import { readFileSync } from 'fs';

const PATH = path.resolve(__dirname, "example.vcard");
const VCARD_DIR_PATH = path.resolve(__dirname, "vcards");
const NUM_PROFILES = 10;

const VCARD_VERSION = "4.0"
const VCARD_HEADER = `BEGIN:VCARD\nVERSION:${VCARD_VERSION}`;
const VCARD_FOOTER = "END:VCARD";

let chooseRandom = (choices: string[]) => {
    return choices[Math.round(Math.random() * choices.length)];
}

async function getBase64(url : string) {
    let response = await axios.get(url, {
        responseType: 'arraybuffer'
    });
    return Buffer.from(response.data, 'binary').toString('base64');
}

let fakeProfilePicture = async (): Promise<string> => {
    let u = `https://picsum.photos/200/300`;
    console.log(`Fetching ${u}`);
    try {
        let data = await getBase64(u);
        console.log(`Got ${data.length} bytes`);
        return data;
    } catch (err) {
        console.error(err);
        return null;
    }
}

let fakeVcard = async (orgs?: string[]): Promise<vCard> => {
    let vc = new vCard();
    let fn = faker.name.firstName();
    let ln = faker.name.lastName();
    let displayName = `${fn} ${ln}`;
    let org = chooseRandom(orgs) || "";
    let imageBytes = await fakeProfilePicture();
    return vCard.fromJSON(
        ["vcard",
            [
                ["version", {}, "text", "4.0"],
                ["n", {}, "text", [ln, fn, "", "Mr.", ""]],
                ["fn", {}, "text", displayName],
                ["org", {}, "text", org],
                ["title", {}, "text", ""],
                // ["photo:data:image/jpeg", ["base64"], [imageBytes]],
                ["tel", { "type": ["work", "voice"] }, "uri", "tel://" + faker.phone.phoneNumber("##########")],
                ["email", {}, "text", faker.internet.email()],
                // ["rev", {}, "timestamp", ""],
            ]
        ]
    );
    return vc
}

let createCards = async () => {
    let orgs = new Array<string>();
    for (let i = 0; i < NUM_PROFILES; ++i) {
        if (Math.random() < 0.5)
            orgs.push(faker.company.companyName());
        orgs.push(null)
    }
    if (!fs.existsSync(VCARD_DIR_PATH)) fs.mkdirSync(VCARD_DIR_PATH);
    for (let i = 0; i < NUM_PROFILES; ++i) {
        // console.log(faker.image.cats());
        let vc = <vCard>await fakeVcard(orgs);
        fs.open(path.resolve(VCARD_DIR_PATH, `${i}.vcf`), "w+", (err : Error, fd :  File) => {
            if (err) {
                console.error(err);
                return;
            }
            fs.writeSync(fd, vc.toString());
        })
    }
}

let init = async () => {
    // let data = fs.readFileSync('./example.vcf');
    // let card = new vCard().parse(data);
    // console.log(card.toJSON());
    await createCards();
    console.log(faker.phone.phoneFormats());
};

init();
