import { GlobalThis } from "../lib/browser";

export type ObjectParams = {
    unique? : boolean,
    multiEntry? : boolean,
    locale? : AutoKeyword | string | null | undefined,
}

export class IndexDef {
    private _indexName: string;
    get indexName () { return this._indexName }

    private _keyPath: string;
    get keyPath () { return this._keyPath }

    private _objectParams: ObjectParams;
    get objectParams () { return this._objectParams }

    constructor(indexName : string, keyPath? : string, objectParams? : ObjectParams) {
        this._indexName = indexName;
        this._keyPath = keyPath || indexName;
        this._objectParams = objectParams || {} as ObjectParams;
    }
}


export class Model {
    private _db  : IDBDatabase;
    get db () { return this._db; }

    private _name : string;
    get name () { return this._name; }
    // set name (name : string) { this._name = name; }

    private _store : IDBObjectStore;
    get store () { return this._store; }

    private _indexDefs : IndexDef [];
    get indexDefs () { return this._indexDefs };

    public constructor(db : IDBDatabase, name : string, indexDefs : IndexDef []) {
        this._db = db;
        this._name = name;
        this._indexDefs = indexDefs;
        this._store = new IDBObjectStore();
    }

    public init() {
        if (this._db.objectStoreNames.contains(this.name)) {
        }
        this.indexDefs.forEach( (indexDef : IndexDef) => {
        })
    }
}