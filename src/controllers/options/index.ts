import { Browser } from "lib/browser";
import { mergeWith } from "lodash";
import stringify from "fast-json-stable-stringify";
import merge from "lodash.merge";
import { stat, Stats } from "fs";

export type ObjectFlags = {
    [key: string]: boolean;
}

export type Options = {
    Runtime: {
        initialTab: string,
        // In minutes.
        statCollectionInterval: number,
    }
    StatCollection: {
        skipN: number,
    },
    Account: {
        collectAccounts: ObjectFlags;
        myAddresses: string[];
        ignoreAddresses: string[];
    }
}

export async function newOptions(browser: Browser): Promise<Options> {
    let collectAccounts = {} as ObjectFlags;
    if (!browser?.accounts) {
        throw new Error(`Browser has no accounts`);
    }
    for (let account of await browser.accounts.list()) {
        collectAccounts[account.id] = true;
    }
    return {
        Runtime: {
            initialTab: 'statistics.overview',
            statCollectionInterval: 60,
        },
        StatCollection: {
            skipN: 0,
        },
        Account: {
            collectAccounts: {} as ObjectFlags,
            myAddresses: new Array<string>(),
            ignoreAddresses: new Array<string>(),
        }
    } as Options;
}

export async function saveOptions(browser: Browser, options: Options) {
    // console.log(`💾 Saving options: ${JSON.stringify(options)}`);
    await browser.storage.local.set({ Options: JSON.parse(JSON.stringify(options)) });
}

export async function getOptions(browser: Browser): Promise<Options> {
    // console.log(`📂 Restoring options`);
    // let tpl = newOptions(browser);
    let opts = ((await browser.storage.local.get()) as any).Options as Options;
    // console.log(opts);

    // Just in case they're using an old version of options, ensure the
    // keys are correct.

    if (!opts.Runtime) {
        opts.Runtime = {
            initialTab: 'statistics.overview',
            statCollectionInterval: 60,
        };
    }

    if (!opts.Runtime.initialTab) {
        opts.Runtime.initialTab = 'statistics.overview';
    }
    opts.Runtime.statCollectionInterval = opts.Runtime.statCollectionInterval || 60;

    if (!opts.Account) {
        opts.Account = {
            collectAccounts: {},
            myAddresses: new Array<string>(),
            ignoreAddresses: new Array<string>(),
        }
    }

    if (!opts.Account.collectAccounts) {
        opts.Account.collectAccounts = {}
    }
    if (!opts.Account.myAddresses) {
        opts.Account.myAddresses = new Array<string>()
    }
    if (!opts.Account.ignoreAddresses) {
        opts.Account.ignoreAddresses = new Array<string>()
    }

    if (!opts.StatCollection) {
        opts.StatCollection = {
            skipN: 0,
        }
    }

    if (!opts.StatCollection.skipN) {
        opts.StatCollection.skipN = 0;
    }

    if (!opts.Account) {
        opts.Account = {
            collectAccounts: {} as ObjectFlags,
            myAddresses: new Array<string>(),
            ignoreAddresses: new Array<string>(),
        }
    }

    if (!opts.Account.collectAccounts) {
        opts.Account.collectAccounts = {}
    }

    if (!opts.Account.myAddresses) {
        opts.Account.myAddresses = new Array<string>();
    }

    if (!opts.Account.ignoreAddresses) {
        opts.Account.ignoreAddresses = new Array<string>();
    }

    // NOT WORKING for some reason
    // return mergeWith(opts, tpl, (value: any, srcValue: any, key: string) => {
    //     console.log(`value = ${stringify(value)}`);
    //     console.log(`srcValue = ${stringify(srcValue)}`);
    // });


    return opts;
}