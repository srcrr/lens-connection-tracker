import { Browser, MessageHeader, ContactNode, AddressBookNode, MailFolder, BrowserPermissions } from "../../lib/browser";
import { v4 as uuidv4 } from 'uuid';
import {toObject, ToObject} from "../util/toobject";
import {
    createModelSchema,
    primitive,
    reference,
    list,
    object,
    identifier,
    serialize,
    deserialize,
    serializable
} from "serializr"

export type NodeType = "addressBook" | "contact" | "mailingList";

class MockBrowserNamespace {
    constructor(
        protected real: Browser,
    ) { }
}

export class _MailFolder {
    static fromJson(data: {[key : string] : any}): _MailFolder {
        let mf = new _MailFolder(
            data['accountId'],
            data['path'],
            data['name'],
            data['type']
        );
        data['_children'].forEach( (childDef : object) => {
            mf._children?.push(_MailFolder.fromJson(childDef));
        });
        return mf;
    }


    public _children? : _MailFolder [];

    constructor(
        public accountId: string,
        public path: string,
        public name?: string,
        public type?: string,
    ) {
        this._children = new Array<_MailFolder>();
    }

    public _addChild(parent: _MailFolder, childName: string): _MailFolder {
        if (this == parent) {
            let mf = new _MailFolder(this.accountId, this.path + "/" + this.name, childName, "imap");
            mf.name = childName;
            this._children?.push(mf);
            return mf;
        }
        for (let child of this._children || []) {
            let added = child._addChild(parent, childName);
            if (added) {
                return added;
            }
        }
        return this;
    }
    _delete(folder: _MailFolder) {
        this._children = this._children?.filter((child) => child != folder);
    }

    public toJson () : object {
        return {
            accountId : this.accountId,
            path: this.path,
            name: this.name,
            type: this.type,
            _children: this._children?.map((child) => child.toJson()),
        }
    }
}

export type MailAccount = {
    folders: _MailFolder[],
    id: string,
    name: string,
    type: "imap" | "nntp" | "pop3",
}

export type _MessageHeader = {
    author: string,
    bccList?: string[],
    ccList?: string[],
    date: Date,
    flagged?: boolean,
    folder?: _MailFolder,
    id: number,
    read: boolean,
    recipients: string[],
    subject: string,
    tags?: string[]
}

export type _MessagePart = {
    _id?: number,
    body?: string,
    contentType?: string,
    headers?: object,
    name?: string,
    partName?: string,
    parts?: _MessagePart,
    size?: number,
}

export type MessageTag = {
    color: string,
    key: string,
    ordinal?: string,
    tag: string,
}

export class _MessageList {
    constructor(
        public id: string | null,
        public messages: _MessageHeader[],
    ) {}
}

export type _QueryInfo = {
    author?: string,
    body?: string,
    flagged?: boolean,
    folder?: _MailFolder,
    fromDate?: Date,
    fromMe?: boolean,
    fullText?: string,
    recipients?: string,
    subject?: string,
    toDate?: Date,
    toMe?: boolean,
    unread?: boolean,
}

export type ContactProperties = {
    FirstName?: string,
    LastName?: string,
    PhoneticFirstName?: string,
    PhoneticLastName?: string,
    DisplayName?: string,
    NickName?: string,
    SpouseName?: string,
    FamilyName?: string,
    PrimaryEmail?: string,
    SecondEmail?: string,
    HomeAddress?: string,
    HomeAddress2?: string,
    HomeCity?: string,
    HomeState?: string,
    HomeZipCode?: string,
    HomeCountry?: string,
    HomePhone?: string,
    HomePhoneType?: string,
    FaxNumber?: string,
    FaxNumberType?: string,
    PagerNumber?: string,
    PagerNumberType?: string,
    CellularNumber?: string,
    CellularNumberType?: string,
    JobTitle?: string,
    Department?: string,
    Company?: string,
    _AimScreenName?: string,
    AnniversaryYear?: string,
    AnniversaryMonth?: string,
    AnniversaryDay?: string,
    BirthYear?: string,
    BirthMonth?: string,
    BirthDay?: string,
    WebPage1?: string,
    WebPage2?: string,
    Custom1?: string,
    Custom2?: string,
    Custom3?: string,
    Custom4?: string,
    Notes?: string,
    PopularityIndex?: string,
    PreferMailFormat?: string,
    PhotoName?: string,
    PhotoType?: string,
    PhotoURI?: string,
};

export type Tab = {};

export type GlobalThis = {
    browser: Browser,
    indexedDB: IDBFactory,
};

export type _ContactNode = {
    id: string,
    properties: ContactProperties,
    type: NodeType,
    parentId?: string,
    readOnly?: boolean,
};

export type MailingListNode = {
    description: string,
    id: string,
    name: string,
    nickname: string,
    type: NodeType,
    contacts?: _ContactNode[],
    parentId?: number,
    readOnly?: boolean,
};

export type _AddressBookNode = {
    id: string,
    name: string,
    type: NodeType,
    contacts: _ContactNode[],
    mailinglists: MailingListNode[],
    parentId?: string,
    readOnly?: boolean,
};


export type Listener = {
    addListener: ((resonse: any) => void),
    removeListener: ((response: any) => void),
  };

export type Port = {
    onMessage: Listener,
    postMessage: (message: any) => void,
};

export class _Accounts {
    static fromJson(data: any): _Accounts {
        return new _Accounts(data['_mailAccounts']);
    }
    toJson() {
        return {
            _mailAccounts : this._mailAccounts,
        };
    }
    constructor(
        public _mailAccounts : Array<MailAccount>,
    ) {}
    public async list(): Promise<MailAccount[]> {
        console.log("Listing mail accounts");
        console.log(this._mailAccounts);
        return this._mailAccounts;
    }
    public get(accountId: string): MailAccount {
        return this._mailAccounts.reduce((prev: any, curr: MailAccount) => {
            return curr.id == accountId ? curr : (prev || null);
        });
    }
};

export class _Folders {
    constructor(
        public _root? : _MailFolder | null,
        accountId? : string,
    ) {
        if (!(_root || accountId)) {
            throw Error(`Must provide either root folder or account ID`);
        }
        if (_root)
            this._root = _root
        else if (accountId)
            this._root = new _MailFolder(accountId, "ROOT");
    }

    static fromJson(data: any): _Folders {
        let f = new _Folders(_MailFolder.fromJson(data['_root']));
        return f;
    }

    public toJson() : object {
        return {
            "_root": this._root?.toJson(),
        }
    }
    public create(parentFolder: _MailFolder, childName: string) {
        this._root?._addChild(parentFolder, childName);
    };
    public rename(folder: _MailFolder, newName: string) {
        folder.name = newName;
    }
    public delete(folder: _MailFolder) {
        if (folder == this._root) {
            this._root = null;
            return;
        }
        folder._delete(folder);
    }
}

export class _MailPage {
    _id = uuidv4();
    messages = new Array<_MessageHeader>();
}

export class _Messages {
    static fromJson(data: any): _Messages {
        let messages = new _Messages();
        messages._mailHeaders = data['_mailHeaders'];
        messages._mailParts = data['_mailParts'];
        messages._pages = data['_pages'];
        messages._myEmail = data['_myEmail'];
        return messages;
    }
    toJson() {
        return {
            _mailHeaders : this._mailHeaders,
            _mailParts : this._mailParts,
            _pages : this._pages,
            _myEmail : this._myEmail,
        };
    }
    _mailHeaders = new Array<_MessageHeader>();
    _mailParts = new Array<_MessagePart>();
    _pages = new Array<_MessageList>();
    _myEmail?: string;
    _queryPages = new Array<_MessageList>(); // ugly
    public async list(folder: _MailFolder): Promise<_MessageList> {
        let i = 0;
        this._mailHeaders.forEach((mailHeader) => {
            if (mailHeader.folder != folder)
                return;
            if (this._pages[i].messages.length >= 100) {
                i += 1;
                this._pages[i] = {
                    id: uuidv4(),
                    messages: new Array<_MessageHeader>(),
                } as _MessageList;
            }
            this._pages[i].messages.push(mailHeader);
        })
        return this._pages[0];
    }
    public async continueList(messageListId: string): Promise<_MessageList> {
        for (let i = 0; i < this._pages.length - 1; ++i)
            if (this._pages[i].id == messageListId)
                return this._pages[i];
        return {
            id: null,
            messages: [],
        } as _MessageList;
    }
    public async get(messageId: number): Promise<_MessageHeader | null> {
        for (let header of this._mailHeaders)
            if (header.id == messageId)
                return header;
        return null;
    }
    public async getFull(messageId: number): Promise<_MessagePart | null> {
        for (let part of this._mailParts)
            if (part._id == messageId)
                return part;
        return null;
    }
    public async query(queryInfo: _QueryInfo): Promise<_MessageList> {
        let i = 0;
        this._pages = new Array<_MessageList>();
        this._mailHeaders.map((header) => header.id).forEach(async (id) => {
            let header = await this.get(id);
            let full = await this.getFull(id);
            let fullText = (!queryInfo.fullText) ? null : (header?.author + "\n"
                + (header?.bccList || [].join(" ")) + "\n"
                + (header?.ccList || [].join(" ")) + "\n"
                + (header?.recipients || [].join(" ")) + "\n"
                + (header?.subject || "") + "\n"
                + (header?.author || "") + "\n"
                + (header?.subject || "") + "\n"
                + (header?.tags || "") + "\n"
                + (full?.body) || "").toLowerCase()
            let doesMatch = (
                (
                    ((queryInfo.author == null) ||
                        (header?.author == queryInfo.author)) &&

                    ((queryInfo.body == null) ||
                        ((full?.body || "").toLowerCase().indexOf(queryInfo?.author || "") >= 0)) &&

                    ((queryInfo.flagged == null) ||
                        (header?.flagged == queryInfo.flagged)) &&

                    ((queryInfo.folder == null) ||
                        (header?.folder == queryInfo.folder)) &&

                    ((queryInfo.fromDate == null) ||
                        (header?.date && (header.date > queryInfo.fromDate))) &&

                    ((queryInfo.toDate == null) ||
                        (header?.date && (header.date < queryInfo.toDate))) &&

                    ((queryInfo.fromMe == null) ||
                        (queryInfo.fromMe && header?.author == this._myEmail)) &&

                    ((queryInfo.fullText == null) ||
                        (fullText && this._myEmail && (fullText.indexOf(this._myEmail) >= 0))) &&

                    ((queryInfo.recipients == null) ||
                        (header && header.recipients.indexOf(queryInfo.recipients) > 0)) &&

                    ((queryInfo.subject == null) ||
                        (queryInfo.subject.indexOf(queryInfo.subject))) &&

                    ((queryInfo.toMe == null) ||
                        (queryInfo.toMe == true && header && this._myEmail && this._myEmail in header.recipients)) &&

                    ((queryInfo.unread == null) ||
                        (queryInfo.unread == (!header?.read)))
                )
            );
            if (doesMatch) {
                if (this._pages[i].messages.length >= 100) {
                    i += i;
                    let qp = new _MessageList(uuidv4(), new Array<_MessageHeader>());
                    this._pages.push(qp);
                }
            }
        });
        return this._pages[0];
    }
}

export class _Contacts {
    static fromJson(data: {[key : string] : any}): _Contacts {
        let c = new _Contacts()
        c._contacts = data['contacts'];
        return c;
    }
    toJson() {
        return {
            contacts: this._contacts,
        }
    }
    _contacts = new Array<_ContactNode>();
    // _addressBooks : _AddressBookNode [];
    public async list(parentId: string): Promise<_ContactNode[]> {
        return this._contacts.filter((contact) => (!parentId) || contact.parentId == parentId);
    }
    public async create(parentId: string, id?: string | null, properties?: ContactProperties): Promise<string> {
        id = id || uuidv4();
        this._contacts.push({
            parentId,
            id,
            properties,
        } as _ContactNode);
        return id;
    }
    public async get(id: string): Promise<_ContactNode | null> {
        for (let contact of this._contacts)
            if (contact.id == id)
                return contact;
        return null;
    }
}

export class _Extension {
    constructor(
        private _url?: string,
    ) {}
    public getURL(path: string): string {
        return this._url + path;
    }
}

export class _AddressBooks {
    static fromJson(data: {[key : string] : any}): _AddressBooks {
        return new _AddressBooks(data['_addressBooks']);
    }
    constructor(
        public _addressBooks: _AddressBookNode[],
    ) {}
    public openUI(): void {
        console.log("It's open! You see it? Hopefully not this is only emulated.");
    };
    public closeUI(): void {
        console.log("Closing UI.");
    }
    public async get(id: string, complete?: boolean): Promise<_AddressBookNode> {
        return this._addressBooks.reduce((prev: any, curr: any) => (curr.id == id && curr.complete == complete ? curr : prev));
    }
    public async list(complete?: boolean): Promise<_AddressBookNode[]> {
        return this._addressBooks;
    }
    public async create(properties: { name?: string }): Promise<string> {
        let id = uuidv4();
        this._addressBooks.push({ id, name: properties.name } as _AddressBookNode);
        return id;
    }
    public update(id: string, properties: { name?: string }): void {
        this._addressBooks.forEach((el) => {
            if (el.id == id && properties.name)
                el.name = properties.name;
        });
    }
    public delete(id: string): void {
        this._addressBooks = this._addressBooks.filter((el) => (el.id != id));
    }
    toJson() {
        return {
            _addressBooks: this._addressBooks,
        }
    }
}

export type StorageKey = null | string | object | string [];

export class _LocalStorage extends MockBrowserNamespace {
    constructor(real : Browser) {
        super(real);
    }
    public async set(keys : object) : Promise<void> {
        if (!(("storage" in this.real) && ("local" in this.real.storage))) return;
        return await this.real.storage.local.set(keys);
    }
    public async get(keys : StorageKey) : Promise<any> {
        if (!(("storage" in this.real) && ("local" in this.real.storage))) return null;
        return await this.real.storage.local.get(keys);
    }
    public async remove(keys : StorageKey) {
        if (!(("storage" in this.real) && ("local" in this.real.storage))) return null;
        return await this.real.storage.local.remove(keys);
    }
}

export class _Storage extends MockBrowserNamespace{
    public local : _LocalStorage;
    constructor(real : Browser, local? : _LocalStorage) {
        super(real)
        this.local = local || new _LocalStorage(this.real);
    }
}

export class _Tabs extends MockBrowserNamespace {
    public async get(tabId: number): Promise<Tab | null> {
        if (!(this.real.tabs)) return null;
        return this.real.tabs.get(tabId);
    }
    public async getCurrent(): Promise<Tab | null> {
        if (!(this.real.tabs)) return null;
        return this.real.tabs.getCurrent();
    }
    public async create(createProperties: { active?: boolean, index?: number, url?: string, windowId?: number }): Promise<Tab | null> {
        if (!(this.real.tabs)) return null;
        return this.real.tabs.create(createProperties);
    }
    public async duplicate(tabId: number): Promise<Tab | null> {
        if (!(this.real.tabs)) return null;
        return this.real.tabs.duplicate(tabId);
    }
}

export type OnMessageType = {
    addListener: (args: any) => void,
}

export class _Runtime extends MockBrowserNamespace {
    public onMessage : OnMessageType = {} as OnMessageType;
    public connectNative(signal: string): Port | null {
        if (!("runtime" in this.real)) return null;
        return this.real.runtime.connectNative(signal);
    }
    public get onConnect() {
        if (!("runtime" in this.real)) return null;
        return this.real.runtime.onConnect;
    }
    public set onConnect(onConnect: any) {
        if (!("runtime" in this.real)) return;
        this.real.runtime.onConnect = onConnect;
    }
    public connect(args: any): Port | null {
        if (!("runtime" in this.real)) return null;
        return this.real.runtime.connect(args);
    }
}

export class _PermissionsSpace extends MockBrowserNamespace {
    constructor(
        real : Browser
    ) {
        super(real);
    }
    public async getAll() : Promise<BrowserPermissions> {
        if (!(this.real.permissions))
            throw new Error("Permissions not given");
        return await this.real.permissions.getAll();
    }
}

export class FakeThunderbirdBrowser extends MockBrowserNamespace implements ToObject {

   toObject = toObject.bind(this);

    private _fakeMeta = {} as {
        addressBookId?: string,
        inbox?: _MailFolder,
        sentbox?: _MailFolder,
        accountId?: string,
        selfEmail?: string,
    };

    browserAction?: any;

    @serializable
    accounts?: _Accounts;

    _folders: _MailFolder[];
    runtime: _Runtime;

    folders?: _Folders;

    messages?: _Messages;

    contacts?: _Contacts;

    addressBooks?: _AddressBooks;
    extension?: _Extension;
    tabs?: _Tabs;
    storage: _Storage;

    permissions? : _PermissionsSpace;

    constructor(real: Browser,
        accounts?: _Accounts,
        _folders?: _MailFolder[] | null, 
        runtime?: _Runtime, 
        folders?: _Folders, 
        messages?: _Messages, 
        contacts?: _Contacts, 
        addressBooks?: _AddressBooks, 
        extension?: _Extension, 
        tabs?: _Tabs, 
        storage?: _Storage,
        permissions?: _PermissionsSpace) {
        super(real);
        this.accounts = accounts;
        this._folders = _folders || new Array<_MailFolder>();
        this.runtime = runtime || new _Runtime(this.real);
        this.folders = folders || new _Folders();
        this.messages = messages || new _Messages();
        this.contacts = contacts || new _Contacts();
        this.addressBooks = addressBooks || new _AddressBooks(new Array<_AddressBookNode>());
        this.extension = extension || new _Extension();
        this.tabs = tabs || new _Tabs(this.real);
        this.storage = storage || new _Storage(this.real);
        this.permissions = permissions || new _PermissionsSpace(this.real);
    }

    public toJson () : object {
        return {
            accounts: this.accounts?.toJson(),
            // runtime: this.runtime.toJson(),
            folders: this.folders?.toJson(),
            messages: this.messages?.toJson(),
            contacts: this.contacts?.toJson(),
            addressBooks: this.addressBooks?.toJson(),
        };
    }

    public static fromJson(real : Browser, data : string | {[key: string] : any}) : FakeThunderbirdBrowser {
        if (typeof(data) == 'string')
            data = JSON.parse(data) as {};
        return new FakeThunderbirdBrowser(
            real,
            _Accounts.fromJson(data['accounts']),
            null,
            new _Runtime(real),
            _Folders.fromJson(data['folders']),
            _Messages.fromJson(data['messages']),
            _Contacts.fromJson(data['contacts']),
            _AddressBooks.fromJson(data['addressBooks']),
        )
    }
};