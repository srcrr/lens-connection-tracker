import { ContactNode, Browser as RealBrowser, MailAccount } from '../lib/browser';
import { emailGrep, listMessages, setDirty, getInternalId } from "./util/index";
import { ProgressListener, Statistics, BeatPointsCollection, Progress, BeatPoints, CompletionListener } from "./analytics";
import { getOptions, Options } from 'controllers/options';

type Browser = RealBrowser;

/**
 * A thread to run statistics within the browser.
 * 
 * This thread saves the state of the statistics after each tick so that they 
 * can be continued.
 */
export class StatisticsRunner {
    private _doPause = false;

    public browser: Browser;
    public date?: Date | null;
    public progressListener?: ProgressListener;
    public statistics?: Statistics;
    public completionListener?: CompletionListener;

    /**
     * Construct a StatisticsRunner.
     * 
     * @param browser The Browser instance (can be FakeBrowser, once it works)
     * @param date The date of the statistics run, default is the current date
     * @param progressListener A class that is is called on each tick of run.
     * @param statistics The instance we're storing. If not given, find the value from the local storage.
     */
    constructor(browser: Browser, date?: Date | null, progressListener?: ProgressListener, completionListener?: CompletionListener, statistics?: Statistics) {
        this.browser = browser;
        this.date = date;
        this.progressListener = progressListener;
        this.completionListener = completionListener;
        this.statistics = statistics || new Statistics(
            {} as BeatPointsCollection,
            new Date(),
            { current: 0, total: 0 } as Progress
        );
    }

    /**
     * Find a contact in the contact book by a given email.
     * 
     * This will look for the email within PrimaryEmail and SecondEmail
     * 
     * @param email The email to serach for.
     */
    private async findContactByEmail(email: string): Promise<ContactNode | null> {
        if (!(this.browser && this.browser.addressBooks)) {
            throw new Error(`Browser does not have address book.`);
        }
        if (!(this.browser && this.browser.contacts)) {
            throw new Error(`Browser does not have contacts.`);
        }
        for (let addressBook of await this.browser.addressBooks.list()) {
            for (let contact of await this.browser.contacts.list(addressBook.id)) {
                if (contact.properties.PrimaryEmail == email || contact.properties.SecondEmail == email) {
                    return contact;
                }
            }
        }
        return null;
    }

    /**
     * Pause the runner. This will save the progress on next tick.
     */
    public async pause(): Promise<void> {
        this._doPause = true;
    }

    /**
     * Save the current state of statistics collection.
     * @param key optional (ignored for now) key to save statistics
     */
    public async saveProgress(key?: string): Promise<void> {
        if (!this.statistics) {
            console.warn("No statistics generated. Not saving.");
            return;
        }
        console.log("saving progress");
        try {
            let obj = { "Statistics": this.statistics.toJson() };
            obj = JSON.parse(JSON.stringify(obj));
            await this.browser.storage.local.set(obj);
        } catch (err) {
            console.error(err);
            console.error(this.statistics.toJson());
            return;
        }
    }

    /**
     * Clear all the saved data.
     */
    public async clearAll(key?: string): Promise<void> {
        await this.browser.storage.local.remove("Statistics");
    }

    /**
     * Resume statistics collection from last saved point.
     * 
     * If nothing is saved, create a new Statistics
     * 
     * @param key (optional, ignored for now) Key for the IndexedDb
     */
    public async resume(key?: any): Promise<void> {
        let statsData = (await this.browser.storage.local.get() as any).Statistics;
        console.log(`Resuming from ${JSON.stringify(statsData)}`);
        if (statsData)
            this.statistics = Statistics.fromJson(statsData);
    }

    private async getOptions(): Promise<Options> {
        return await getOptions(this.browser);
    }

    private async getOptionSkipN(): Promise<number> {
        return Number((await getOptions(this.browser)).StatCollection?.skipN || 0);
    }

    private async shouldScanAccount(account: MailAccount) {
        return (await this.getOptions()).Account.collectAccounts[account.id];
    }

    private async shouldIgnoreEmail(address: string) {
        let re: RegExp | string = address;
        let ignored = (await this.getOptions()).Account.ignoreAddresses;
        let myAddresses = (await this.getOptions()).Account.myAddresses;
        if (myAddresses.includes(address)) return true;
        for (let ignore of ignored) {
            re = new RegExp(ignore);
            if ((re as any) != {}) {
                // re.compile();
                console.log(`regex: ${JSON.stringify(re)} matches '${address}' ? ${address.match(re)}`);
                if (address.match(re)) return true;
            } else {
                console.log(`ignore ('${ignore}') == address ('${address}') ? ${ignore == address}`);
                if (ignore == address) return true;
            }
        }
        return false;
    }

    /**
     * Calculate the total number of messages, then analyze each email message, updating the progress as we go.
     */
    public run = async (): Promise<Statistics | undefined> => {
        let beatPoints = {} as BeatPointsCollection;
        let messageCount = 0;
        let thisRunner = this;

        let progress = {

        } as Progress;

        console.log(this.browser);

        if (!(this.browser?.accounts)) {
            throw new Error(`Browser has no property 'accounts'`);
        }

        if (this.statistics && !(this.statistics?.scannedMessages)) {
            this.statistics.scannedMessages = new Array<string>();
        }

        // First get the total number of messages.
        if ((this.statistics?.progress?.total || 0) == 0 || (this.statistics?.progress?.current || 0) > 0) {
            for (let account of (await this.browser.accounts.list())) {
                if (!(await this.shouldScanAccount(account))) {
                    console.log(`User wants to skip this account (${account.name})`);
                    continue;
                }
                for (let folder of account.folders) {
                    for await (let item of listMessages(this.browser, folder)) {
                        // console.log(`<Account='${account.name}', folder='${folder.name}', message='${item?.subject}'>`);
                        if (this._doPause) {
                            return this.statistics;
                        }
                        try {
                            if (this.progressListener)
                                this.progressListener(-1, messageCount, {instance: this});
                            ++messageCount;
                            if (this.statistics && this.statistics.progress)
                                this.statistics.progress.total = messageCount;
                        }
                        catch (err) {
                            console.error(`While fetching item ${item?.id}`);
                            console.error(err);
                        }
                    }
                }
            }
        }
        console.log(`There are ${messageCount} messages`);

        /**
         * Now comes the hard work.
         * 
         * 1. Go through each message in each folder in each account in the 
         * browser.
         * 
         *     A. Go through each recipient.
         *     B. Find the recipient in the address book.
         *     C. If the recipient is not found, don't do anything.
         *     D. Otherwise, add the contact to the beat points.
         * 
         * 2. Do the same thing as 1, but for the message author (there's only 
         * one author per message)
         */
        let n = 0;
        for (let account of (await this.browser.accounts.list())) {
            if (!(await this.shouldScanAccount(account))) {
                continue;
            }
            for (let folder of account.folders) {
                for await (let item of listMessages(this.browser, folder)) {
                    let message = item;
                    if (!message)
                        continue;
                    ++n;


                    // Basically pick up where we left off
                    if (this.statistics && this.statistics.progress && n < (this.statistics.progress.current || 0)) {
                        continue;
                    }

                    // Skip if the message was already scanned.
                    // TODO: add override for new contact (maybe?)
                    try {
                        let internalId = await getInternalId(this.browser, message);
                        if (internalId in (this.statistics?.scannedMessages || []))
                            continue;
                    } catch (err) {
                        console.error(`Could not fetch internal ID for message ${message.id}`);
                        console.error(`- subject: ${message.subject}`);
                        console.error('- message:');
                        try {
                            console.error(await this.browser.messages?.getFull(message.id));
                        } catch (err2) {
                            console.error(`While trying to fetch message`);
                            console.error(err2);
                        }
                        continue;
                    }

                    // Completely exit out if the user requested a pause.
                    if (this._doPause) {
                        console.log("User requested pause.");
                        return this.statistics;
                    }

                    // Skip any messages that do not fall in line with "skip every <n>" options
                    // (But allow for the *last* message so that the progress registers as 100%)
                    let skipN = await (this.getOptionSkipN());
                    if ((n <= (this.statistics?.progress?.total || 1) - 1) && (n % skipN !== 0)) {
                        continue;
                    }

                    if (this.statistics?.progress)
                        this.statistics.progress.current = n;
                    // console.log(`${n} / ${messageCount} (${Math.round(n/messageCount*100)}%)`);
                    if (this.progressListener)
                        this.progressListener(n, messageCount, {instance: this});
                    // Get the recipients and put in the "received" bucket.
                    for (let recipient of message.recipients) {
                        let recipientEmail = emailGrep(recipient);
                        if (await this.shouldIgnoreEmail(recipientEmail)) {
                            console.log(`Email ignored: ${recipientEmail}`);
                            continue;
                        }
                        // console.log(`search recipient '${recipientEmail}'`);
                        let contact = await this.findContactByEmail(recipientEmail);
                        // If the contact was found in the address book, add the address book ID.
                        if (contact && this.statistics) {
                            this.statistics.beats[contact.id] = beatPoints[contact.id] || {} as BeatPointsCollection;
                            if (!this.statistics.beats[contact.id]) {
                                this.statistics.beats[contact.id] = {
                                    sent: {} as BeatPoints,
                                    received: {} as BeatPoints,
                                };
                            }
                            if (!this.statistics.beats[contact.id].received) {
                                this.statistics.beats[contact.id].received = this.statistics.beats[contact.id].received || {} as BeatPoints;
                            }
                            this.statistics.beats[contact.id].received[message.id] = message.date;
                        } else if (this.statistics) {
                            // Otherwise, add the contact to the bucket.
                            let bucket = ((await this.browser.storage.local.get() as any).ContactBucket || new Array<String>()) as string[];
                            // If the contact is not in the bucket, add it, and
                            // save the bucket to local storage.
                            if (!bucket.includes(recipientEmail)) {
                                bucket.push(recipientEmail);
                                await this.browser.storage.local.set({ ContactBucket: bucket });
                            }
                            let bucketI = bucket.indexOf(recipientEmail);
                            let bucketId = "bucket://" + bucketI;
                            if (!this.statistics.beats[bucketId]) {
                                this.statistics.beats[bucketId] = {
                                    sent: {} as BeatPoints,
                                    received: {} as BeatPoints,
                                };
                            }
                            if (!this.statistics.beats[bucketId].received) {
                                this.statistics.beats[bucketId].received = this.statistics.beats[bucketId].received || {} as BeatPoints;
                            }
                            this.statistics.beats[bucketId].received[message.id] = message.date
                        }
                    }
                    // Add the author to "sent"
                    let authorEmail = emailGrep(message.author);
                    // console.log(`search sender '${authorEmail}'`);
                    let contact = await this.findContactByEmail(authorEmail);
                    if (await this.shouldIgnoreEmail(authorEmail)) {
                        console.log(`Email ignored: ${authorEmail}`);
                        continue;
                    }
                    if (this.statistics && contact) {
                        if (!this.statistics.beats[contact.id]) {
                            this.statistics.beats[contact.id] = {
                                sent: {} as BeatPoints,
                                received: {} as BeatPoints,
                            };
                        }
                        this.statistics.beats[contact.id] = this.statistics.beats[contact.id] || {} as BeatPointsCollection;
                        this.statistics.beats[contact.id].sent = this.statistics.beats[contact.id].received || {} as BeatPoints;
                        this.statistics.beats[contact.id].sent[message.id] = message.date;

                    } else if (this.statistics) {
                        // Otherwise, add the contact to the bucket.
                        let bucket = ((await this.browser.storage.local.get() as any).ContactBucket || new Array<String>()) as string[];
                        // If the contact is not in the bucket, add it, and
                        // save the bucket to local storage.
                        if (!bucket.includes(authorEmail)) {
                            bucket.push(authorEmail);
                            await this.browser.storage.local.set({ ContactBucket: bucket });
                        }
                        let bucketI = bucket.indexOf(authorEmail);
                        let bucketId = "bucket://" + bucketI;
                        if (!this.statistics.beats[bucketId]) {
                            this.statistics.beats[bucketId] = {
                                sent: {} as BeatPoints,
                                received: {} as BeatPoints,
                            };
                        }
                        if (!this.statistics?.beats[bucketId].sent) {
                            this.statistics.beats[bucketId].sent = this.statistics.beats[bucketId].sent || {} as BeatPoints;
                        }
                        this.statistics.beats[bucketId].sent[message.id] = message.date
                    }
                    // Finally, mark the message as having been analyzed.
                    if (this.statistics?.scannedMessages) {
                        let internalId = await getInternalId(this.browser, message);
                        if (internalId)
                            this.statistics.scannedMessages.push(internalId);
                    }
                }
            }
        }
        if (!this.statistics) {
            this.statistics = new Statistics(
                beatPoints,
                new Date(),
                {} as Progress,
            );
        }
        if (this.completionListener) this.completionListener();
        await setDirty(this.browser);
        return this.statistics || null;
    }
}
