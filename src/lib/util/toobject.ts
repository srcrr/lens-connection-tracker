
/**
 * https://stackoverflow.com/a/59383428
 */

export const toObject = function(original : {[key: string] : any}) {
    original = original || {};
    let keys = Object.keys(original);
    return keys.reduce((classAsObj : {[key : string] : any}, key) => {
      if (typeof original[key] === 'object' && original[key].hasOwnProperty('toObject') )
        classAsObj[key] = original[key].toObject();
      else if (typeof original[key] === 'object' && original[key].hasOwnProperty('length')) {
        classAsObj[key] = [];
        for (var i = 0; i < original[key].length; i++) {
          if (typeof original[key][i] === 'object' && original[key][i].hasOwnProperty('toObject')) {
            classAsObj[key].push(original[key][i].toObject());
          } else {
            classAsObj[key].push(original[key][i]);
          }
        }
      }
      else if (typeof original[key] === 'function') { } //do nothing
      else
        classAsObj[key] = original[key];
      return classAsObj;
    }, {})
  }

  export interface ToObject {
    toObject: Function;
  }