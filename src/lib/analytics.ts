import PACKAGE from "../../package.json";
import { getMessages, MessageProxy } from "./messages";
import { Browser as _Browser, MessageHeader, AddressBookNode, MailFolder, QueryInfo, MailAccount } from '../lib/browser';
import { authorIsMe } from "./messages";
import moment from "moment";
import { FakeThunderbirdBrowser, _MessageHeader, _QueryInfo, _MailFolder } from "./fake/browser";
import stringify from "fast-json-stable-stringify";
import { object } from "serializr";

export type Browser = _Browser | FakeThunderbirdBrowser;
type JSONObject = { [key: string]: any }

function checkProperty(browser: Browser, prop: string) {
    if (!(prop in browser)) {
        throw new Error(`'${prop}' not in browser`);
    }
}

/**
 * An association graph.
 * 
 * Shows a 1-D relationship based on a common object.
 * 
 * ```
 *    _______________________
 *   /  node    node   node  \
 *  |      \     |    /       |
 *  | node - (center) - node  |
 *  |       /     |    \      |
 *   \   node    node   node /
 * ```
 */
export interface IRing {
    nodes: IRingNode[];
}

export interface IRingNode {
    ring: IRing;
    calculateRelevancy(data: any): Promise<number>;
}

export class EventRingNode implements IRingNode {
    constructor(
        public ring: EventRing
    ) { }
    async calculateRelevancy(data: any): Promise<number> {
        throw new Error("Method not implemented.");
    }
}

export class EventRing implements IRing {
    constructor(
        public nodes: EventRingNode[],
    ) { }
}

export class MailRingNode implements IRingNode {
    constructor(
        public ring: MailRing,
        public messageId: number,
        public browser?: Browser,
        public relevancy?: number,
    ) {
        this.ring = ring;
        this.messageId = messageId;
        this.browser = browser;
    }

    async calculateRelevancy(): Promise<number> {
        if (!(this.browser && this.browser.messages)) {
            throw new Error("`browser` does not have `messages` instance.");
        }
        let thisMessage = await this.browser.messages.get(this.messageId);
        if (!thisMessage) {
            throw Error(`Could not find message with ID ${this.messageId}`)
        }
        let message = this.ring.center;
        if (thisMessage == message)
            return 1;
        if (moment(thisMessage.date).isBefore(message.date)) {
            let earliestMessage = await this.ring.getEarliestMessage();
            if (!earliestMessage) {
                throw new Error(`Could not get earliest message`);
            }
            let earliestDiff = moment(earliestMessage.date).diff(message.date);
            let thisDiff = moment(thisMessage.date).diff(message.date);
            return thisDiff / earliestDiff;
        }
        let latestMessage = await this.ring.getLatestMessage();
        if (!latestMessage) {
            throw new Error(`Could not get latest message`);
        }
        let latestDiff = moment(latestMessage.date).diff(message.date);
        let thisDiff = moment(thisMessage.date).diff(message.date);
        this.relevancy = thisDiff / latestDiff;
        return this.relevancy;
    }
}

export class MailRing implements IRing {

    constructor(
        public center: MessageHeader,
        public nodes: MailRingNode[],
        public browser?: Browser,
    ) { }

    async run(): Promise<void> {
        return this.nodes.forEach(async (node) => {
            await node.calculateRelevancy();
        });
    }

    async getEarliestMessage(): Promise<MessageHeader | null> {
        if (!(this.browser && this.browser.messages)) {
            throw new Error("`browser` does not have `messages` instance.");
        }
        let earliestMessage: MessageProxy | null = null;
        for (let messageProxy of await getMessages(this.browser)) {
            let curr = messageProxy.header.date;
            if (!earliestMessage || moment(curr).isBefore(earliestMessage.header.date)) {
                earliestMessage = messageProxy;
            }
        }
        return earliestMessage?.header || null;
    }

    async getLatestMessage(): Promise<MessageHeader | null> {
        if (!(this.browser && this.browser.messages)) {
            throw new Error("`browser` does not have `messages` instance.");
        }
        let latestMessage: MessageProxy | null = null;
        for (let messageProxy of await getMessages(this.browser)) {
            let curr = messageProxy.header.date;
            if (!latestMessage || moment(curr).isAfter(latestMessage.header.date)) {
                latestMessage = messageProxy;
            }
        }
        return latestMessage?.header || null;
    }
}

export class RingData {
    constructor(
        public id: string,
        public mailRing: MailRing,
        public eventRing: EventRing,
    ) { }
}

export class ContactBeat {
    constructor(
        public contactId: string,
        public times: Date[],
        public browser?: Browser,
    ) { }

    static async construct(browser: Browser, contactId: string, account: MailAccount, mode: "sent" | "received") {
        let times = new Array<Date>();

        // Retrieve the "inbox"
        // TODO: make this a setting
        let inbox: null | MailFolder = null;
        for (let mailFolder of account.folders) {
            if (mailFolder && mailFolder.name && mailFolder.name.toLowerCase().indexOf("inbox") > 0)
                inbox = mailFolder;
        }

        // Retrieve the "sentbox"
        // TODO: make this a setting
        let sentbox: null | MailFolder = null;
        for (let mailFolder of account.folders) {
            if (mailFolder && mailFolder.name && mailFolder.name.toLowerCase().indexOf("sent") > 0)
                sentbox = mailFolder;
        }

        if (!browser.contacts) {
            throw new Error(`Browser has not contacts`);
        }

        if (!browser.messages) {
            throw new Error(`Browser has not messages`);
        }

        let contact = await browser.contacts.get(contactId);
        if (!contact) {
            throw new Error(`Could not find contact id {${contactId}} in account "${account.name}"`)
        }
        let contactEmail = contact.properties.PrimaryEmail || contact.properties.SecondEmail;

        if (sentbox) {
            let queryInfo = {
            } as QueryInfo;
            if (mode == "sent") {
                queryInfo.folder = sentbox;
                queryInfo.recipients = contactEmail;
            } else if (mode == "received") {
                queryInfo.author = contactEmail;
            }
            let page = await browser.messages.query(queryInfo);
            page?.messages.forEach((message: MessageHeader) => {
                times.push(message.date);
            });
            while (page?.id) {
                page = await browser.messages.continueList(page.id);
                page?.messages.forEach((message) => {
                    times.push(message.date);
                });
            }
        }

        return new ContactBeat(contactId, times, browser);
    }
}

export type BeatPoints = {
    [messageId: string]: Date
};

export type BeatPointsCollection = {
    [contactId: string]: {
        sent: BeatPoints,
        received: BeatPoints,
    }
}

/**
 * A simple progress counter.
 */
export type Progress = {
    current: number,
    total: number,
}

export type ProgressListener = (current: number, total: number, data?: any) => Promise<any | void>;
export type CompletionListener = (...data: any[]) => Promise<any | void>;

export type TimeSpanMark = {
    [i: number]: number,
}

export type TimeDataDef = {
    months: TimeSpanMark,
    weekDays: TimeSpanMark,
    hours: TimeSpanMark,
}

export type TimeData = {
    sent: TimeDataDef,
    received: TimeDataDef
}

/**
 * Data about message & contact usage.
 * Contains the following information:
 * 
 * :beats:
 *     Information on when a contact (or email) sent a message.
 * 
 * :lastRun:
 *     When statistics were last run.
 * 
 * :progress:
 *     How far we've gone (in case collection was interrupted)
 * 
 * :timeData:
 *     Time slots for message sending/receiving.
 * 
 * :scannedMessages:
 *     A list containing message IDs of scanned messages.
 */
export class Statistics {
    public toJson(): JSONObject {
        return {
            beats: this.beats,
            lastRun: this.lastRun,
            progress: this.progress,
            timeData: this.timeData,
            scannedMessages: this.scannedMessages,
        }
    }

    static fromJson(statsData: JSONObject): Statistics {
        return new Statistics(
            statsData['beats'] as BeatPointsCollection,
            statsData['lastRun'],
            statsData['progress'],
            statsData['timeData'],
        );
    }

    static async fromLocalStorage(browser: Browser, key?: string) {
        return Statistics.fromJson(
            (await browser.storage.local.get('Statistics') as any).Statistics
        );
    }

    public static NAME = "ANALYTICS";
    constructor(
        public beats: BeatPointsCollection,
        public lastRun?: Date,
        public progress?: Progress,
        public scannedMessages?: string[],
    ) {
        this.scannedMessages = this.scannedMessages || new Array<string>();
    }

    public timeData(): TimeData {
        let d = {
            sent: {
                months: new Array<number>(12),
                weekDays: new Array<number>(7),
                hours: new Array<number>(24),
            },
            received: {
                months: new Array<number>(12),
                weekDays: new Array<number>(7),
                hours: new Array<number>(24),
            },
        } as TimeData;

        // Fill each one with zeros
        for (let k in d as any) {
            for (let l in (d as any)[k]) {
                ((d as any)[k][l] as number []).fill(0);
            }
        }
        console.log(`Collecting time data...`);

        for (let beat of Object.values(this.beats)) {
            for (let direction of ['sent', 'received']) {
                let dates: Date[];
                if (!(beat as any)[direction])
                    continue;
                try {
                    // console.log(`Fetching '${direction}' of ${stringify(beat)}`);
                    dates = Object.values((beat as any)[direction] as BeatPoints);
                } catch (err) {
                    console.error(err);
                    continue;
                }
                for (let point of dates || []) {
                    if (!point) continue;
                    // let r = (d as any)[direction] as TimeDataDef;
                    let ph = point.getHours();
                    let pw = point.getDate();
                    let pm = point.getMonth();
                    ((d as any)[direction] as TimeDataDef).hours[ph] += 1;
                    ((d as any)[direction] as TimeDataDef).weekDays[pw] += 1;
                    ((d as any)[direction] as TimeDataDef).months[pm] += 1;
                    // console.log(d);
                }
            }
        }

        return d;
    }
}

export type FrequencyField = "second" | "minute" | "hour" | "day" | "month" | "year";
export class Frequency {

    constructor(
        public field: FrequencyField,
        public interval: number,
        public repetitions: number,
        public parent?: Frequency | null,
        public child?: Frequency | null,
    ) {
        this.child = child || null;
        this.parent = child || null;
    }


    public set(field: FrequencyField, interval: number, repetitions: number) {
        let instance = new Frequency(field, interval, repetitions, this);
        instance.child = this;
        return instance;
    }

    public remove(frequency: Frequency): Frequency | null {
        if (this == frequency && this.child) {
            this.child.parent = this.parent;
            return this.parent || null;
        } else if (this == frequency && !this.parent) {
            if (this.child) {
                this.child.parent = null;
            }
            return this.child || null;
        } else if (this == frequency && !this.child) {
            return null;
        } else if (this.child) {
            this.child.remove(frequency);
            return null;
        }
        return null;
    }
}

export class GoalProgress extends Frequency {

    constructor(
        public field: FrequencyField,
        public interval: number,
        public repetitions: number,
        public completed?: number,
        public parent?: Frequency,
        public child?: Frequency,
    ) {
        super(field, interval, repetitions, parent, child);
    }
}

export class Goal {
    constructor(
        public contact: string,
        public frequency: Frequency,
        public progress: GoalProgress,
    ) { }
}

// export class Profile {
//     constructor(
//         public analytics: Analytics,
//         public goals: Goal[],
//         public potentialAssociations: RingData,
//     ) { }
// }

// export function writeAnalytics(browser: Browser, analytics: Analytics) {
//     let item = { "Analytics": browser };
//     browser.storage.local.set(item);
// }

// export async function getAnalytics(browser: Browser): Promise<Analytics> {
//     let analyticsData = (await browser.storage.local.get({})).Analytics;
//     return Analytics.fromJson(analyticsData);
// }